import torch
from torch import nn
import torch.nn.functional as F


def conv_block(in_channels, out_channels,
               kernel: int = 5,
               stride: int = 2,
               padding: int = 2,
               dropout_rate: float = 0.0):
    return nn.Sequential(
        nn.Conv2d(in_channels, out_channels,
                  kernel_size=(kernel, kernel),
                  stride=(stride, stride),
                  padding=padding),
        nn.ReLU(inplace=True),
        nn.BatchNorm2d(out_channels),
        nn.Dropout(p=dropout_rate, inplace=True)
    )


class VariationalEncoder(nn.Module):
    def __init__(self, latent_dims: int, device):
        super(VariationalEncoder, self).__init__()
        self.conv1 = nn.Conv2d(1, 8, 3, stride=2, padding=1)  # conv_block(1, 16)
        self.conv2 = nn.Conv2d(8, 16, 3, stride=2, padding=1)  # conv_block(16, 32)
        self.batch2 = nn.BatchNorm2d(16)
        self.conv3 = nn.Conv2d(16, 32, 3, stride=2, padding=0)  # conv_block(32, 64)
        self.linear1 = nn.Linear(3 * 3 * 32, 128)  # nn.Linear(12*12*64, 512)
        self.linear2 = nn.Linear(128, latent_dims)
        self.linear3 = nn.Linear(128, latent_dims)

        self.N = torch.distributions.Normal(0, 1)
        self.N.loc = self.N.loc.cuda() # hack to get sampling on the GPU
        self.N.scale = self.N.scale.cuda()
        self.kl = 0
        self.device = device

    def forward(self, x):
        x = x.to(self.device)
        x = F.relu(self.conv1(x)) # self.conv1(x)  #
        x = F.relu(self.batch2(self.conv2(x)))   # self.conv2(x)  #
        x = F.relu(self.conv3(x))  # self.conv3(x)  #
        x = torch.flatten(x, start_dim=1)
        x = F.relu(self.linear1(x))
        mu = self.linear2(x)
        sigma = torch.exp(self.linear3(x))
        z = mu + sigma*self.N.sample(mu.shape)
        self.kl = (sigma**2 + mu**2 - torch.log(sigma) - 1/2).sum()
        return z


class Decoder(nn.Module):

    def __init__(self, latent_dims: int, device):
        super().__init__()

        self.decoder_lin = nn.Sequential(
            nn.Linear(latent_dims, 128),  # 512
            nn.ReLU(True),
            nn.Linear(128, 3 * 3 * 32),  # nn.Linear(512, 12 * 12 * 64),
            nn.ReLU(True)
        )

        self.unflatten = nn.Unflatten(dim=1, unflattened_size=(32, 3, 3))  # (64, 12, 12)

        self.decoder_conv = nn.Sequential(
            # nn.ConvTranspose2d(64, 32, 3, stride=2, padding=1, output_padding=1),
            nn.ConvTranspose2d(32, 16, 3, stride=2, output_padding=0),
            # nn.BatchNorm2d(32),
            nn.BatchNorm2d(16),
            nn.ReLU(True),
            # nn.ConvTranspose2d(32, 16, 3, stride=2, padding=1, output_padding=1),
            nn.ConvTranspose2d(16, 8, 3, stride=2, padding=1, output_padding=1),
            # nn.BatchNorm2d(16),
            nn.BatchNorm2d(8),
            nn.ReLU(True),
            # nn.ConvTranspose2d(16, 1, 3, stride=2, padding=1, output_padding=1)
            nn.ConvTranspose2d(8, 1, 3, stride=2, padding=1, output_padding=1)
        )
        self.device = device

    def forward(self, x):
        x = x.to(self.device)
        x = self.decoder_lin(x)
        x = self.unflatten(x)
        x = self.decoder_conv(x)
        x = torch.sigmoid(x)
        return x


class VariationalAutoencoder(nn.Module):
    def __init__(self, latent_dims: int, device):
        super(VariationalAutoencoder, self).__init__()
        self.encoder = VariationalEncoder(latent_dims, device)
        self.decoder = Decoder(latent_dims, device)
        self.device = device

    def forward(self, x):
        x = x.to(self.device)
        z = self.encoder(x)
        return self.decoder(z)
