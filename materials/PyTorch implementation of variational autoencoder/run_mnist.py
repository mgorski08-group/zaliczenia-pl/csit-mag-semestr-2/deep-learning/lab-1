from pathlib import Path

from loguru import logger
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import plotly.express as px
from sklearn.manifold import TSNE
import torch
import torchvision
from torchvision import transforms
from torch.utils.data import DataLoader, random_split
from tqdm import tqdm

import model.vae_net as model


def train_epoch(vae, device, dataloader, optimizer):
    # Set train mode for both the encoder and the decoder
    vae.train()
    train_loss = 0.0
    # Iterate the dataloader (we do not need the label values, this is unsupervised learning)
    for x, _ in dataloader:
        # Move tensor to the proper device
        x = x.to(device)
        x_hat = vae(x)
        # Evaluate loss
        loss = ((x - x_hat)**2).sum() + vae.encoder.kl

        # Backward pass
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        # Print batch loss
        logger.debug('\t partial train loss (single batch): %f' % (loss.item()))
        train_loss += loss.item()

    return train_loss / len(dataloader.dataset)


def test_epoch(vae, device, dataloader):
    # Set evaluation mode for encoder and decoder
    vae.eval()
    val_loss = 0.0
    with torch.no_grad():  # No need to track the gradients
        for x, _ in dataloader:
            # Move tensor to the proper device
            x = x.to(device)
            # Encode data
            # Decode data
            x_hat = vae(x)
            loss = ((x - x_hat) ** 2).sum() + vae.encoder.kl
            val_loss += loss.item()

    return val_loss / len(dataloader.dataset)


def plot_ae_outputs(encoder, decoder, test_dataset, device, n=5):
    plt.figure(figsize=(10, 4.5))
    for i in range(n):
        ax = plt.subplot(2, n, i + 1)
        img = test_dataset[i][0].unsqueeze(0).to(device)
        encoder.eval()
        decoder.eval()
        with torch.no_grad():
            rec_img = decoder(encoder(img))
        plt.imshow(img.cpu().squeeze().numpy(), cmap='gist_gray')
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
        if i == n // 2:
            ax.set_title('Original images')
        ax = plt.subplot(2, n, i + 1 + n)
        plt.imshow(rec_img.cpu().squeeze().numpy(), cmap='gist_gray')
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
        if i == n // 2:
            ax.set_title('Reconstructed images')
    plt.show()


def show_image(img):
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)))


def plot_reconstructed(decoder, latent_dim=4, n=10, device='cuda'):
    plt.figure(figsize=(8.5, 8.5))
    w = 28
    img = np.zeros((n*w, n*w))

    with torch.no_grad():
        # sample latent vectors from the normal distribution
        latent = torch.randn(n*n, latent_dim, device=device)

        # reconstruct images from the latent vectors
        img_recon = decoder(latent)
        img_recon = img_recon.cpu().numpy()

    for i in range(n):
        for j in range(n):
            img[(n-1-i)*w:(n-1-i+1)*w, j*w:(j+1)*w] = img_recon[i * n + j]
    plt.imshow(img, cmap='gist_gray')
    plt.show()


def main():
    data_dir = 'dataset'

    train_dataset = torchvision.datasets.MNIST(data_dir, train=True, download=True)
    test_dataset = torchvision.datasets.MNIST(data_dir, train=False, download=True)

    train_transform = transforms.Compose([
        transforms.ToTensor(),
    ])

    test_transform = transforms.Compose([
        transforms.ToTensor(),
    ])

    train_dataset.transform = train_transform
    test_dataset.transform = test_transform

    m = len(train_dataset)
    train_data, val_data = random_split(train_dataset, [int(np.floor(m - m * 0.2)), int(np.ceil(m * 0.2))])
    batch_size = 256

    train_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size)
    valid_loader = torch.utils.data.DataLoader(val_data, batch_size=batch_size)
    # test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=batch_size, shuffle=True)

    torch.manual_seed(0)

    d = 4
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")

    vae = model.VariationalAutoencoder(latent_dims=d,
                                       device=device)
    lr = 1e-3
    optimizer = torch.optim.Adam(vae.parameters(), lr=lr, weight_decay=1e-5)

    logger.info(f'Selected device: {device}')

    vae.to(device)

    num_epochs = 25
    for epoch in range(num_epochs):
        train_loss = train_epoch(vae, device, train_loader, optimizer)
        val_loss = test_epoch(vae, device, valid_loader)
        logger.debug('\n EPOCH {}/{} \t train loss {:.3f} \t val loss {:.3f}'.format(epoch + 1,
                                                                                     num_epochs,
                                                                                     train_loss,
                                                                                     val_loss))
        if (epoch + 1) % 10 == 0:
            plot_ae_outputs(vae.encoder, vae.decoder, test_dataset, device, n=5)
            pass

    vae.eval()

    plot_reconstructed(vae.decoder)

    encoded_samples = []
    for sample in tqdm(test_dataset):
        img = sample[0].unsqueeze(0).to(device)
        label = sample[1]
        # Encode image
        with torch.no_grad():
            encoded_img = vae.encoder(img)
        # Append to list
        encoded_img = encoded_img.flatten().cpu().numpy()
        encoded_sample = {f"Enc. Variable {i}": enc for i, enc in enumerate(encoded_img)}
        encoded_sample['label'] = label
        encoded_samples.append(encoded_sample)

    encoded_samples = pd.DataFrame(encoded_samples)

    px.scatter(encoded_samples, x='Enc. Variable 0', y='Enc. Variable 1', color=encoded_samples.label.astype(str),
               opacity=0.7)

    tsne = TSNE(n_components=2)
    tsne_results = tsne.fit_transform(encoded_samples.drop(['label'], axis=1))

    fig = px.scatter(tsne_results, x=0, y=1, color=encoded_samples.label.astype(str),
                     labels={'0': 'tsne-2d-one', '1': 'tsne-2d-two'})
    fig.show()


if __name__ == '__main__':
    main()

